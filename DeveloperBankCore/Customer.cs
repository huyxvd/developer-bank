﻿public class Customer
{
    public int Number { get; set; }
    public string FullName { get; set; }
    public decimal WithDrawalAmount { get; set; }
}
